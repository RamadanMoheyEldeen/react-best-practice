import React from 'react';
import './App.css';
import Fragment from './topics/fragment/container/'
import Result from './components/result/'
import Button from './components/button/'
import Fetch from './topics/fetch/'
import Bind from './topics/bind/'
import Reusable from './topics/reusable/container'
import LifeCycle from './topics/lifeCycle/parent'
import Hoc from './topics/HOC/'

class App extends React.Component {
  state = { componentName: "React best Practice" }

  changeComponent = selectedComponent => {
      var component = null;
      switch(selectedComponent){
        case "Fragment":
          component = <Fragment />
          break;
        case "Fetch":
            component = <Fetch />
            break;
        case "Bind":
            component = <Bind />
            break;
        case "Component LifeCycle":
            component = <LifeCycle />
            break;
        case "HOC" :
            component = <Hoc />
            break;
        case "Reusable Components":
            component = <Reusable/>
            break;
        default:
          component = this.state.componentName
      }

      this.setState(prevstate => ({
        componentName: component
      }))
  }

  componentDidMount(){
    this.changeComponent()
  }

  render() {
    return (

      <div className="App">
        <header className="App-header">
              <div className="buttonsContainer">
                    <Button className="App-link" css={"button"} selectedComponent={"Reusable Components"} onClickFunction={this.changeComponent} />
                    <Button className="App-link" css={"button"} selectedComponent={"Component LifeCycle"} onClickFunction={this.changeComponent} />
                    <Button className="App-link" css={"button"} selectedComponent={"HOC"} onClickFunction={this.changeComponent} />
                    <Button className="App-link" css={"button"} selectedComponent={"Fragment"} onClickFunction={this.changeComponent} />
                    <Button className="App-link" css={"button"} selectedComponent={"Fetch"} onClickFunction={this.changeComponent} />
                    <Button className="App-link" css={"button"} selectedComponent={"Bind"} onClickFunction={this.changeComponent} />
              </div>
              <Result componentName={this.state.componentName} />
        </header>
      </div>
    )
  }
}

export default App
