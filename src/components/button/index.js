import React from 'react';

export default class Button extends React.Component {
  handleClick = () => {
    this.props.onClickFunction(this.props.selectedComponent)
  }

  render() {
    return (
      <button className={this.props.css} onClick={this.handleClick}>
        {this.props.selectedComponent}
      </button>
    )
  }
}
