import React from 'react'
import axios from 'axios'
import Button from '../../components/button/'

const Card = props => {
  return (
    <div onClick={()=>{props.onDelete(props.id)}} style={{ margin: '1em' }}>
      <img alt="avatar" style={{ width: '100px' }} src={props.avatar_url} />
      <div>
        <div style={{ fontWeight: 'bold' }}>{props.name}</div>
        <div>{props.blog}</div>
      </div>
    </div>
  )
}

const CardList = props => {
  return <div>{props.cards.map(card => <Card key={card.id} onDelete={props.deleteCard} {...card} />)}</div>
}

class Entery extends React.Component {
  state = {
    userName: ''
  }

  handleSubmit = () => {
    axios
      .get(`https://api.github.com/users/${this.state.userName}`)
      .then(resp => {
        this.props.onSubmit(resp.data)
        this.setState({ userName: '' })
      }).catch(err=> {
        alert("User Not Found")
        this.setState({ userName: '' })
      })
  }

  render() {
    return (
      <>
        <input
          className="input"
          type="text"
          value={this.state.userName}
          onChange={event => this.setState({ userName: event.target.value })}
          placeholder="GitHub username"
          required
        />
        <Button css='addButton' selectedComponent={"Add card"} onClickFunction={this.handleSubmit}></Button>
      </>
    )
  }
}

class GitHub extends React.Component {
  state = {
    cards: []
  }

  addNewCard = cardInfo => {
    this.setState(prevState => ({
      cards: prevState.cards.concat(cardInfo)
    }))
  }

  delete = id =>{
    this.setState(prevState => ({
        cards: prevState.cards.filter(card => card.id != id)
    }))
  }

  render() {
    return (
      <div>
        <p className="p">{"How to Deal with this Reference Inside the Promise"}</p>
        <Entery onSubmit={this.addNewCard} />
        <CardList  deleteCard={this.delete} cards={this.state.cards} />
      </div>
    )
  }
}

export default GitHub
