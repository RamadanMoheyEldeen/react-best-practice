import React from 'react'
class Child extends React.Component {
  constructor(props){
    console.log('Child Constructor')
    super(props)

  }

  componentWillReceiveProps(){
    console.log("wil recive props child")
  }

  componentWillMount(){
    console.log("Child componentWillMount")
  }

  componentDidMount(){
    console.log("Child componentDidMount")
  }



  shouldComponentUpdate(){
    console.log("Child shouldComponentUpdate")
    return true;
  }

  render(){
    console.log("Child render")
    return  <p>{this.props.counter}</p>
  }
}

export default Child;
