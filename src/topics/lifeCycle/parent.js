import React from 'react'
import Child from './child'
import Button from '../../components/button'
class Parent extends React.Component {
      constructor(props){
        console.log("constructor")

      super(props)
      this.state ={
          counter: 1 
      }
    }

    componentWillMount(){
        console.log("willMount")
    }

    componentDidMount(){
      console.log("didMount")
  }

  increase = () =>
    this.setState(prevState=> ({
      counter: prevState.counter + 1
    })) 

  render(){
    console.log("render")
      return  <>
                <p className="p">{"Component Life Cycle"}</p>
                <p>{this.state.counter}</p>
                <Child counter={this.state.counter}/>
                <Button className="App-link" css={"button"} selectedComponent={"Icrease"} onClickFunction={this.increase} />


              </>
  }
}

export default Parent ;
