import React from 'react';
import Columns from '../component/';


const header = {name: 'Name' , title: 'Title'}
const data = [
    {"name": "Bishoy ", "title": "Backend team lead"},
    {"name": "Bola", "title": "Backend/DevOps"}
]

class Fragment extends React.Component {
  render(){
    return <>
                <p className="p">{"Fragment"}</p>
                <table className="table">
                    <tbody>
                    <tr className="row">
                        <Columns color row={header} />
                    </tr>
                    {
                          data.map((item , index)=>{
                                  return  <tr className="row" key={index}><Columns   row={item} /></tr>
                        })
                    }
                    </tbody>
               </table>

               <p>{` <Columns /> would need to return multiple <td> elements in order for the rendered HTML to be valid.
                    If a parent div was used inside the render() of <Columns />, then the resulting HTML will be invalid.
              `}</p>
           </>


  }
}

export default Fragment;
