import React from 'react'
import Button from '../../components/button'
class Bind extends React.Component {
  state = {
  }

  constructor(props){
    super(props)
    this.bindOnConstructor=this.bindOnConstructor.bind(this)
  }

  bindFromClassProperty = () => {
      this.setState(prevState=>({
          text: !prevState.text ? "I am bind from a class property" :  "" ,
          showClassProperty: !prevState.showClassProperty
      }))
  };

  bindInsideRender(){
    this.setState(prevState=>({
        textRender: !prevState.textRender ? "I am bind on render " :  "",
        showRender: !prevState.showRender

    }))
  }


  arrowFunction(){
    this.setState(prevState=>({
        textArrow: !prevState.textArrow ?  "I am bind using arrow function" : "",
        showArrow: !prevState.showArrow

    }))
  }

  bindOnConstructor(){
    this.setState(prevState=>({
        textConstructor: !prevState.textConstructor ? "I am bind using Constructor" : "",
        showConstructor: !prevState.showConstructor

    }))
  }

  render() {
    return (
      <div>
          <p className="p">{"Here is bind techniques"}</p>
          <Button className="App-link" css={"button"} selectedComponent={"Inside Render"} onClickFunction={this.bindInsideRender.bind(this)} />
          {this.state.showRender ? <p className="pContent">{this.state.textRender}</p> : <p/>}
          <Button className="App-link" css={"button"} selectedComponent={"Arrow Function"} onClickFunction={() => this.arrowFunction()} />
          {this.state.showArrow ? <p className="pContent">{this.state.textArrow}</p>: <p/> }
          <Button className="App-link" css={"button"} selectedComponent={"On Constructor"} onClickFunction={this.bindOnConstructor} />
          {this.state.showConstructor ? <p className="pContent">{this.state.textConstructor}</p>: <p/>}
          <Button className="App-link" css={"button"} selectedComponent={"Class Property"} onClickFunction={this.bindFromClassProperty} />
          {this.state.showClassProperty ? <p className="pContent">{this.state.text}</p> : <p/>}


      </div>
    )
  }
}

export default Bind
