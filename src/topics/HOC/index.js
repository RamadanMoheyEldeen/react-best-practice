import React from 'react'

import preload from './employees.json'
import withSearch from './withsearch'
import EmployeeCard from './employeeCard'

const Employee = (props) => {
  const { searchTerm } = props
  
  return (
    <div>

      <div>
        {preload
          .filter(employee => `${employee.name} ${employee.title}`.toUpperCase().indexOf(searchTerm.toUpperCase()) >= 0)
          .map(employee => <EmployeeCard key={employee.id} {...employee} />)}
      </div>
    </div>
  )
}
export default withSearch(Employee)