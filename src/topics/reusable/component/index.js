import React from 'react';

const ReusableComponent = (props) => <>
                       <p className="p" onClick={props.slideBack}>{"Create Tiny Components That Just Do One Thing"}</p>
                       {props.isTitleVisible ? <p onClick={props.showItem}>{"Some hints to know if you should separate your component:"}</p> : '' }
                       {props.isItemVisible ?  <p className="pContent" onClick={props.slide}>{props.num == 7 ? props.item : props.num + " - "+ props.item}</p> : '' }
                                    </>
export default ReusableComponent;