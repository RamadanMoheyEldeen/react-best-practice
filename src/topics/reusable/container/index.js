import React from 'react';
import ReusableComponent from '../component/'

class Reusable extends React.Component {
      state = {
        isTitleVisible: false,
        isItemVisible: false,
        selectedItem: 0,
    }

    showItem = () => 
        this.setState(prevState => ({ isItemVisible: true }))

    slideBack = () => 
        this.setState(prevState => ({
            selectedItem:  prevState.selectedItem > 0 ?  prevState.selectedItem - 1 : 0,
            isTitleVisible: true
        }))
    
    slide = () => 
        this.setState(prevState => ({
            selectedItem:  prevState.selectedItem == items.length-1 ? 6 : prevState.selectedItem +1 }))

    render() {
        return <ReusableComponent slideBack={this.slideBack}
                                  showItem={this.showItem}
                                  slide={this.slide}
                                  isTitleVisible={this.state.isTitleVisible}
                                  isItemVisible={this.state.isItemVisible}
                                  item={items[this.state.selectedItem]}
                                  num={this.state.selectedItem + 1}
          />
    }
}

export default Reusable;

const items = [
    `Keep your render function small and simple. You can split the render function into several 
     functions in the same component, but if it makes sense try splitting those render chunks into 
     separate components.`,
    `Are you dealing with application state logic and also presentation in the same component? Try
     to separate it into a component that just handles the state and one or several components that
     handle the presentation, where the state is passed as a property to the child components.`,
    `Do you have some elements in your render method that need to be updated much more often than others?
     Extract that chunk into a separate component, and try to localize the state updates inside 
     (if it makes sense), or implement shouldComponentUpdate in the other components to avoid unnecessary 
     re-renders.`,
    `Do you have a form with lots of sections and fields but you really don’t care about all the
     intermediate state, just the initial and on submit value? Extract that form into its own component 
     (or even different sections of the form into their own components) that accept the initial 
     values as properties, and have an onSubmit callback property that is called with the final values 
     of the form, making the parent not need to re-render every time an intermediate change in the form 
     occurs. Internally in those section components, either use uncontrolled components or use the component
     internal state.`,
    `Are you doing a lot of styling for layouting a set of components? Extract that into layout components 
     that have configurable but predefined layouts and place your components as children of them.`,
    `Are you adding behavior to some component via state like hovering, collapsing, expanding, or making 
     it pop over? Move that behavior into separate components that accept a single children which is a function 
     to render, and sets the state via params to that function, like:
    `
    ,
    `<Hoverable>
     {hovered => <div>{hovered ? “I’m hovered!” : “I’m not hovered”}</div>}
    </Hoverable>
    `

]